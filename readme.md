## Alicia Wallet
* A digital wallet to use for everyday financial transactions.

## Requirements
* PHP >= 5.5.9
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* php cURL extension

## Installation Guide
* install composer (see https://getcomposer.org/doc/00-intro.md)
* Copy the project folder in your server root
* Goto the project folder(check wheather `composer.json` file is exists) and run below command
    ```
    $ composer update
    ```
    or
    ```
    $ php composer.phar update
    ```
* You may need to configure some permissions. Directories within the  `storage` and the `bootstrap/cache` directories should be writable by your web server. If you are using the Homestead virtual machine, these permissions should already be set.

* after setting the DB configuration in `.env` file run below command to create tables
    ```
    $ php artisan migrate --force
    ```

## Third party library packages configs
* All the configs you find in `.env` file. Later Everything will be updatable     from admin panel.

    * For Database
        * DB_CONNECTION=mysql
        * DB_HOST=127.0.0.1
        * DB_DATABASE=digital_wallet
        *DB_USERNAME=root
        * DB_PASSWORD=root
        
    * For Twilio 
        * TWILIO_SID=AC888edbc130753635bb249d794c89c
        * TWILIO_TOKEN=16206360aaaa8d3925d3e0b697e
        * TWILIO_FROM=+12055896864
        
    * For SMTP MAIL (for now if you using gmail)
        * MAIL_DRIVER=smtp
        * MAIL_HOST=smtp.gmail.com
        * MAIL_PORT=465
        * MAIL_USERNAME=youremail@gmail.com
        * MAIL_PASSWORD=password
        * MAIL_ENCRYPTION=ssl
        * MAIL_FROM=youremail@gmail.com
        * MAIL_NAME=yourname
        
    * For Facebook 
        * FACEBOOK_APP_ID=19194879825862737
        * FACEBOOK_APP_SECRET=8e4a9487391a00f80092d9401f758881b
        
    * For Stripe 
        * STRIPE_API_KEY=sk_test_DYlk7MVVDNsdfa4zpeVO4sDjJwq
        
    * For Paypal
        * PAYPAL_CLIENT_ID=Abi3t-ED76WNwMH8k07hISLmKO-Y8SZZ0KMNGq28jR-76HK0Gg1cuhDuxweJuUkVFZ-Y495Qj8XzyPNS
        * PAYPAL_SECRET_KEY=EDrcIsfKvtVN_9lNn-D2v1PXm9BkxvVUCpydXcUnhN8DSLSytDIu9hYU1zf642TmEw48XrgkRQe_q2o0
        * PAYPAL_MODE_LIVE=false | true

