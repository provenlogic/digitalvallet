<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trans_id', 256);
           /* $table->bigInteger('order_id');*/
            $table->decimal('amount', 10, 2)->default(0.00);
            $table->enum('currency', ['USD', 'MYR'])->default('USD');
            $table->enum('gateway', ['STRIPE', 'PAYPAL', 'SENANGPAY'])->default('STRIPE');
            $table->text('extra_info')->nullable();
            $table->string('status', 100);

            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
        });
    }
}
