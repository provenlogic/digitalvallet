<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('trans_table_id')->default(0);
            $table->bigInteger('other_user_id')->nullable();
            $table->decimal('amount', 10, 2)->default(0.00);
            $table->enum('currency', ['USD', 'MYR'])->default('USD');
            $table->enum('order_type', ['ADD_MONEY', 'SEND_MONEY', 'BANK_TRANSFER', 'SAVINGS_ADDED'])->default('ADD_MONEY');
            $table->enum('status', ['SUCCESS', 'FAILED', 'PENDING'])->default('SUCCESS');
            $table->string('status_reason', 255)->nullable();
            $table->string('remarks', 256)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
