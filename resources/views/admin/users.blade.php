@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Users Management
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Users</span>
                        </div>
                        <div class="box-tools">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-flat dropdown-toggle no-background-color no-border-color" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-bars"></i>
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li class="action" data-action-type="ACTIVATE_USER">
                                        <a href="javascript:void(0)">Activate Selected</a>
                                    </li>
                                    <li class="action" data-action-type="DEACTIVATE_USER">
                                        <a href="javascript:void(0)">Deactivate Selected</a>
                                    </li>
                                    <li class="action" data-action-type="DELETE_USERS">
                                        <a href="javascript:void(0)">Delete users</a>
                                    </li>
                                </ul>
                            </div>
                            
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered" id="user-table">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="label--checkbox">
                                            <input type="checkbox" 
                                                    class="checkbox" id="check-all-user">
                                        </label>
                                    </th>
                                    <th>Name</th>
                                    <th>Mobile No.</th>
                                    <th>Email</th>
                                    <th>Balance</th>
                                    <th>Joined On</th>
                                    <th>Signed With</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                    
                                @foreach($users as $user)

                                <tr>
                                    <td>
                                        <label class="label--checkbox">
                                            <input type="checkbox" 
                                                    class="checkbox check-each-user" 
                                                    data-user-id="{{$user->id}}">
                                        </label>
                                    </td>
                                    <td>{{$user->fullName()}}</td>
                                    <td @if($user->is_mobile_verified == 0) style="color:red" title="Mobile not verified" @endif>{{$user->fullMobileno()}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->balance}}</td>
                                    <td>{{$user->joinedOn()}}</td>
                                    <td style="text-align:center;font-size:20px">
                                        {!! $user->registerdFrom() !!}
                                    </td>
                                    <td style="text-align:center">{!! $user->activationStatus() !!}</td>
                                </tr>

                                @endforeach


                            </tbody>
                        </table>
                        <div class="col-md-12" style="text-align:center">
                            {!! $users->appends(request()->all())->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection 
@section('top-scripts')
<style type="text/css">
    #user-table_filter > label
    {
    padding: 10px;
   /* background: rgba(0, 0, 0, 0.01);*/
    border-radius: 2px;
    margin-top: 5px;
    margin-left: 0px;
    font-weight: normal;
    margin-bottom: 12px;
    /*border: 1px solid rgba(0, 0, 0, 0.07);*/
    }
    #user-table_filter > label > input
    {
    margin-left: 5px;
    width: 300px;
    height: 44px;
    border: none;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
    transition: box-shadow 200ms cubic-bezier(0.4, 0.0, 0.2, 1);
    font: 16px arial,sans-serif;
    padding-left: 5px;
    }
    #user-table_filter > label > input:focus
    {
    outline: none;
    }
    .dataTables_empty
    {
    display: none;
    }
    #user-table_length, #user-table_info, #user-table_paginate
    {
    display: none;
    }
    .dropdown-menu > li > a
    {
    text-transform: uppercase;
    }
</style>
@endsection 

@section('scripts')
@parent
<script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        
        $('#user-table').DataTable({
            "pageLength": 100
        });




        $(".action").on('click', function(){

            var elem = $(this);
            var action_type = elem.data('action-type');
            var user_ids = checkedUsers.join();

            var data = {
                user_ids : user_ids,
                action_type : action_type
            };

            $.post("{{url('admin/users/status/change')}}", data, function(response){

                if(response.success) {
                    toastr.success(response.text);
                    setTimeout(function(){
                        window.location.reload();
                    }, 1000);
                    
                } else {
                    toastr.error('Action request failed');
                }

            });


        });






        var checkedUsers = [];

        $("#check-all-user").on('change', function(){

            var checked = $(this).is(':checked');

            $(".check-each-user").each(function(){
                $(this).prop("checked", checked);
                $(this).trigger('change');
            });

        });


       $(".check-each-user").on('change', function(){

            var elem = $(this);
            var userId = elem.data('user-id');

            var index = checkedUsers.indexOf(userId);

            if(this.checked && index == -1) {
                checkedUsers.push(userId);
            } else if(!this.checked && index > -1) {
                checkedUsers.splice(index, 1);
            }

       });



    });
</script>
@endsection