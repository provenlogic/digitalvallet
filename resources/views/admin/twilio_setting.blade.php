@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Twilio Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Twilio Api Key Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "twilio-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Twilio SID</label>
                                <input type="text"  placeholder="Enter Twilio Sid" name = "twilio_sid" value = "{{$twilio_sid}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Twilio Token</label>
                                <input type="text" placeholder="Enter Twilio Token" name = "twilio_token" value = "{{$twilio_token}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Twilio From(number)</label>
                                <input type="text" placeholder="Enter Twilio Number" name = "twilio_from" value = "{{$twilio_from}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

         <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Test Twilio SMS</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "twilio-settings-sms-test">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Mobile No</label>
                                <input type="text"  placeholder="Enter Mobile No(with +country code)" name = "mobile_no" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Message Text</label>
                                <input type="text" placeholder="Enter message body" name = "message" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SEND</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){
    
    
        $("#twilio-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/twilio/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });


        $("#twilio-settings-sms-test").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/twilio/test')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error(response.text);
                }
    
            });
    
    
        });


    
    
    
    
    });
    
    
</script>
@endsection