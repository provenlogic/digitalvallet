@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            IFTTT Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>IFTTT FITBIT Activity Recipe Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "ifttt-fitbit-activity-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label>Webhook : <span style="text-decoration: underline">{{url('ifttt/webhooktest')}}</span>(METHOD: GET)</label>
                            </div>
                            
                            <div class="form-group">
                                <label class="package-label">IFTTT FITBIT Mobile No</label>
                                <input type="text"  placeholder="Enter IFTTT FITBIT mobile no" name = "ifttt_fitbit_mobile_no" value = "{{$ifttt_fitbit_mobile_no}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">IFTTT FITBIT Country Code</label>
                                <input type="text"  placeholder="Enter Fitbit client secret" name = "ifttt_fitbit_country_code" value = "{{$ifttt_fitbit_country_code}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Fitbit IFTTT Amount</label>
                                <input type="text"  placeholder="Fitbit IFTTT Amount" name = "ifttt_fitbit_amount" value = "{{$ifttt_fitbit_amount}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>IFTTT EMAIL Recipe Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "ifttt-email-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label>Webhook : <span style="text-decoration: underline">{{url('ifttt/webhooktest/email?subject=')}}subject</span>(METHOD: GET)</label>
                            </div>
                            
                            <div class="form-group">
                                <label class="package-label">Mobile No</label>
                                <input type="text"  placeholder="" name = "ifttt_email_mobile_no" value = "{{$ifttt_email_mobile_no}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Country Code</label>
                                <input type="text"  placeholder="" name = "ifttt_email_mobile_country_code" value = "{{$ifttt_email_mobile_country_code}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Email subject</label>
                                <input type="text"  placeholder="" name = "ifttt_email_subject" value = "{{$ifttt_email_subject}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">SMS Body</label>
                                <input type="text"  placeholder="" name = "ifttt_email_sms_body" value = "{{$ifttt_email_sms_body}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
    </section>
</div>
@endsection
@section('scripts')
@parent

<script type="text/javascript">
    $(document).ready(function(){
    
    
        $("#ifttt-fitbit-activity-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/ifttt/fitbit/activity-recipe/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });



        $("#ifttt-email-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/ifttt/email-recipe/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });
    

    
    
    
    
    
    });
    
    
</script>
@endsection