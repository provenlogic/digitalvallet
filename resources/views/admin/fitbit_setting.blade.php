@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Fitbit Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Fitbit App Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "fitbit-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label>Fitbit Authrization callback url : <span style="text-decoration: underline">{{url('fitbit/callback')}}</span></label>
                                <label>Fitbit Subscription Webhook url : <span style="text-decoration: underline">{{url('fitbit/webhook/activities')}}</span></label>
                            </div>
                            
                            <div class="form-group">
                                <label class="package-label">Fitbit Client ID</label>
                                <input type="text"  placeholder="Enter Fitbit client ID" name = "fitbit_client_id" value = "{{$fitbit_client_id}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Fitbit Client Secret</label>
                                <input type="text"  placeholder="Enter Fitbit client secret" name = "fitbit_client_secret" value = "{{$fitbit_client_secret}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Fitbit App Subscription Webhook Verify Code</label>
                                <input type="text"  placeholder="Enter Fitbit Subscription webhook verify code" name = "fitbit_subscription_webhook_verify_code" value = "{{$fitbit_subscription_webhook_verify_code}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Fitbit setps count</label>
                                <input type="text"  placeholder="Enter Setps" name = "fitibt_steps_count" value = "{{$fitibt_steps_count}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Each step goal amount</label>
                                <input type="text"  placeholder="Enter amount for each step goal" name = "fitibt_steps_goal_amount" value = "{{$fitibt_steps_goal_amount}}" class="form-control">
                            </div>
                            
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    
    </section>
</div>
@endsection
@section('scripts')
@parent

<script type="text/javascript">
    $(document).ready(function(){
    
    
        $("#fitbit-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/fitbit/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });
    

    
    
    
    
    
    });
    
    
</script>
@endsection