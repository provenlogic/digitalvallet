@extends('admin.layouts.main') 
@section('content') @parent
<div class="content-wrapper">
    <section class="content-header content-header-custom">
        <h1 class="content-header-head">
            Stripe Settings
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Stripe Api Key Settings</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form method = "POST" id = "stripe-settings-form">
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="package-label">Stripe Api Server key</label>
                                <input type="text"  placeholder="Stripe Api Server key" name = "stripe_api_server_key" value = "{{$stripe_api_server_key}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="package-label">Stripe Api Public Key</label>
                                <input type="text" placeholder="Enter Stripe Api Public Key" name = "stripe_api_public_key" value = "{{$stripe_api_public_key}}" class="form-control">
                            </div>
                            <button type="submit" class="btn btn-block btn-success btn-flat margin-top-5px">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-widget">
                    <div class="box-header">
                        <div class="user-block">
                            <span>Test Stripe</span>
                        </div>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="/charge" method="post" id="payment-form"  style="margin-bottom:10px">
                            <div class="col-md-10 no-padding">
                                <div id="card-element"></div>
                                <div id="card-errors"></div>
                            </div>
                            <div class="col-md-2 no-padding text-center">
                                <button type="submit" class="btn btn-success btn-block btn-flat" style="height: 42px;">Test With Card</button>
                            </div>
                        </form>
                        <div class="col-md-12 no-padding">
                            <br>
                            <label>Response</label>
                            <textarea class="form-control" id="test-response" style="height:350px"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('top-scripts')
<style type="text/css">
    /**
    * The CSS shown here will not be introduced in the Quickstart guide, but shows
    * how you can use CSS to style your Element's container.
    */
    .StripeElement {
        background-color: white;
        padding: 8px 12px;
        border-radius: 4px;
        border: 1px solid transparent;
        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }
    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }
    .StripeElement--invalid {
        border-color: #fa755a;
    }
    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>
<script src="https://js.stripe.com/v3/"></script>
@endsection
@section('scripts')
@parent
<script type="text/javascript">
    $(document).ready(function(){
    
    
        $("#stripe-settings-form").on("submit", function(event){
    
    
            event.preventDefault();
    
            var data = $(this).serializeArray();
    
            $.post("{{url('admin/settings/stripe/save')}}", data, function(response){
    
                if(response.success) {
                    toastr.success(response.text);
                } else {
                    toastr.error("Failed to save");
                }
    
            });
    
    
        });
    
    
    
        var stripe = Stripe($("input[name='stripe_api_public_key']").val());
    
        // Create an instance of Elements
        var elements = stripe.elements();
    
        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                lineHeight: '24px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };
    
        // Create an instance of the card Element
        var card = elements.create('card', {style: style});
    
        // Add an instance of the card Element into the `card-element` <div>
        card.mount('#card-element');
    
        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
                if (event.error) {
                    displayError.textContent = event.error.message;
                } else {
                    displayError.textContent = '';
                }
        });
    
    
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();
    
            stripe.createToken(card).then(function(result) {
                if (result.error) {
                  // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
               
                    runTest(result.token.id);
                }
            });


        });
    
    
    
        function runTest(charge_token)
        {
        
            var data = {
                charge_token : charge_token,
                amount : 1,
                currency : 'USD',
                description : 'Test payment with 1 dollar',
                _token : "{{csrf_token()}}"
            };
        
            $.post("{{url('admin/settings/stripe/test')}}", data, function(response){
        
                var prety = JSON.stringify(response.data, undefined, 4);
        
                $("#test-response").text(prety);
        
                toastr.info('Test completed');
        
            });
        
        }
    
    
    
    
    
    });
    
</script>
@endsection