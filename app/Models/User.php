<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    protected $fillable = [
        'first_name', 
        'last_name',
        'username',
        'password',
        'mobile_no', 
        'country_code',
        'is_mobile_verified', 
        'email', 
        'balance',
        'currency',
        'register_from',
        'data_incomplete',
        'status',
        'last_access_time',
        'last_access_ip',
        'saving_amount'
    ];

    protected $hidden = ['password'];

    public function getTableName()
    {
        return $this->table;
    }


    public function fullMobileno()
    {
        return $this->country_code.$this->mobile_no;
    }


    public function fullName()
    {
        return $this->first_name.' '.$this->last_name;
    }


    public function joinedOn()
    {
        return date('d.m.Y', strtotime($this->created_at));
    }


    public function registerdFrom()
    {
        switch ($this->register_from) {
            case 'NORMAL':
                return '<span title="Registered manually">MANUAL</span>';
                break;

            case 'FACEBOOK':
                return '<i class="fa fa-facebook-official" title="Registered with Facebook"></i>';
                break;

            case 'GOOGLE':
                return '<i class="fa fa-google-plus-official" title="Registered with Google"></i>';
                break;
        }
    }


    public function activationStatus()
    {
        switch ($this->status) {
            case 'ACTIAVATED':
                return '<i class="fa fa-circle" style="color: green;cursor:pointer" title="Activated"></i>';
                break;

            case 'DEACTIVATED_BY_HIMSELF':
                return '<i class="fa fa-circle" style="color:#FF9800;cursor:pointer" title="Deactivated by himself"></i>';
                break;

            case 'DEACTIVATED_BY_ADMIN':
                return '<i class="fa fa-circle" style="color: red;cursor:pointer" title="Deactivated by admin"></i>';
                break;
        }
    }


}
