<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = [
        'trans_id',
        'amount',
        'currency',
        'gateway',
        'extra_info',
        'status'
    ];

    public function getTableName()
    {
        return $this->table;
    }

}