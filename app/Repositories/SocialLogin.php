<?php

namespace App\Repositories;

use App\Repositories\RegisterRepository;
use App\Models\User;
use App\Models\SocialLogin as SocialLoginModel;

class SocialLogin 
{

	protected $first_name = "";
	protected $last_name  = "";
	protected $socialID   = "";
	protected $userEmail  = "";
	protected $provider   = "";
	protected $isDataIncomplete = false;


	const FACEBOOK = 'FACEBOOK';
	const GOOGLE    = 'GOOGLE';


	public function __construct(
		User $user, 
		SocialLoginModel $socialLogin, 
		RegisterRepository $registerRepo
	)
	{
		$this->user = $user;
		$this->socialLogin = $socialLogin;
		$this->registerRepo = $registerRepo;
	}

	public function setProvider($provider)
	{
		$this->provider = $provider;
	}

	public function setSocialID($id)
	{
		$this->socialID = $id;
		return $this;
	}


	public function setUserEmail($email)
	{
		$this->userEmail = $email;
		return $this;
	}


	public function setFirstname($fName)
	{
		$this->first_name = $fName;
		return $this;
	}

	public function setLastname($lName)
	{
		$this->last_name = $lName;
		return $this;
	}

	public function isDataIncomplete()
	{
		return $this->isDataIncomplete;
	}


	protected function findUserWithSocialID($sid)
	{
		$user = $this->user->join(
			$this->socialLogin->getTableName(), $this->user->getTableName().'.id',
			'=',
			$this->socialLogin->getTableName().'.user_id'
		)
		->where('social_account_id', $sid)
		->where('social_provider', $this->provider)
		->select([$this->user->getTableName().'.*'])
		->first();

		return $user;
	}


	protected function findUserWithEmail($email)
	{
		return $this->user->where('email', $email)->first();
	}


	protected function createSocialEntry($userID, $socialID)
	{
		$socialLogin = new $this->socialLogin;
        $socialLogin->user_id = $userID;
        $socialLogin->social_account_id = $socialID;
        $socialLogin->social_provider = $this->provider;
        $socialLogin->save();
        return $socialLogin;
	}



	public function getStoredUser($socialID, $email)
	{

		$user = $this->findUserWithSocialID($socialID);
		if($user) {
			$this->isDataIncomplete = $this->registerRepo->isDataIncomplete($user);
			return $user;
		}

		$user = $this->findUserWithEmail($email);
		if($user) {
			$this->createSocialEntry($user->id, $socialID);
			$this->isDataIncomplete = $this->registerRepo->isDataIncomplete($user);
			return $user;
		}

		return null;
	}


	public function registerUser()
	{
		$user = $this->registerRepo->createUser([
            'first_name'       => $this->first_name,
            'last_name'        => $this->last_name,
            'email'            => $this->userEmail,
            'register_from'    => $this->provider,
            'last_access_time' => date('Y-m-d H:i:s'),
            'last_access_ip'   => request()->ip()
        ]);

		$this->createSocialEntry($user->id, $this->socialID);
        $this->isDataIncomplete = $this->registerRepo->isDataIncomplete($user);

     	return $user;
	}

}