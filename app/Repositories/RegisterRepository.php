<?php

namespace App\Repositories;

use App\Models\User;
use Validator;
use Hash;

class RegisterRepository
{

	public function __construct(User $user)
	{
		$this->user = $user;
	}


    public function isDataIncomplete($user = null)
    {
        if(!$user) {
            return true;
        } else if(is_null($user->first_name) || empty($user->first_name)) {
            return true;
        }  else if(is_null($user->last_name) || empty($user->last_name)) {
            return true;
        } else if(is_null($user->mobile_no) || empty($user->mobile_no)) {
            return true;
        } else {
            return false;
        }
    }


	public function validateRegisterData($data, &$errors) 
	{

        $validator = Validator::make($data, [
            'first_name'   => 'required|max:256',
            'last_name'    => 'required|max:256',
            'password'     => 'required|min:8|max:100',
            'mobile_no'    => 'required|numeric|unique:users,mobile_no',
            'country_code' => 'required|numeric',
            'email'        => 'email|unique:users,email'  
           /* 'otp_code'     => 'required|min:4|max:4'*/
        ]);

        if($validator->fails()) {
            $errors = $this->formatRegisterErrors($validator->errors());
            return false;
        }

        return true;
    } 


    public function formatRegisterErrors($errors) 
    {
        $msg = [];
        ($errors->has('first_name')) ? $msg['first_name']     = $errors->get('first_name')[0] : '';
        ($errors->has('last_name')) ? $msg['last_name']       = $errors->get('last_name')[0] : '';
        ($errors->has('password')) ? $msg['password']         = $errors->get('password')[0] : '';
        ($errors->has('mobile_no')) ? $msg['mobile_no']       = $errors->get('mobile_no')[0] : '';
        ($errors->has('country_code')) ? $msg['country_code'] = $errors->get('country_code')[0] : '';
        ($errors->has('email')) ? $msg['email']               = $errors->get('email')[0] : '';
        /*($errors->has('otp_code')) ? $msg['otp_code']         = $errors->get('otp_code')[0] : '';*/
        return $msg;
    }



    public function createUser($data)
    {
    	return $this->user->create($data);
    }



    protected function emailLoginRules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:8|max:100'
        ];
    }


    protected function mobileLoginRules()
    {
        return [
            'mobile_no' => 'required|numeric|exists:users,mobile_no',
            'password' => 'required|min:8|max:100'
        ];
    }


    public function validateLoginData($data, &$errors = []) 
    {

        if(isset($data['login_by_email']) && $data['login_by_email'] == 1) {
            
            $validator = Validator::make($data, $this->emailLoginRules());
            if($validator->fails()) {
                $errors = $this->formatLoginErrors($validator->errors(), true);
                return false;
            }

        } else {

            $validator = Validator::make($data, $this->mobileLoginRules());
            if($validator->fails()) {
                $errors = $this->formatLoginErrors($validator->errors());
                return false;
            }
        }

        return true;
    }


    public function formatLoginErrors($errors, $isLoginByEmail = false) 
    {
        $msg = [];

        ($errors->has('password')) ? $msg['password'] = $errors->get('password')[0] : '';
        if($isLoginByEmail) {
            ($errors->has('email')) ? $msg['email'] = $errors->get('email')[0] : '';
        } else {
            ($errors->has('mobile_no')) ? $msg['mobile_no'] = $errors->get('mobile_no')[0] : '';
        }

        return $msg;
    }



    public function login($username, $password, $isLoginByEmail = false)
    {

        if($isLoginByEmail) {
            $user = $this->user->where('email', $username)->first();
        } else {
            $user = $this->user->where('mobile_no', $username)->first();
        }

        return Hash::check($password, $user->password) ? $user : false;

    }






    protected function updateUserRules($userID)
    {

        return $this->updateRules = [
            'mobile_no'    => 'sometimes|required',
            'country_code' => 'sometimes|required',
            'password'     => 'sometimes|required|min:8|max:100',
            'first_name'   => 'sometimes|required|max:256',
            'last_name'    => 'sometimes|required|max:256',
            'email'        => 'sometimes|required|email|unique:users,email,'.$userID

        ];
   
    }


    public function formatUpdateErrors($errors) 
    {
        $msg = [];

        $errors->has('password') ? $msg['password']         = $errors->get('password')[0] : '';
        $errors->has('mobile_no') ? $msg['mobile_no']       = $errors->get('mobile_no')[0] : '';
        $errors->has('country_code') ? $msg['country_code'] = $errors->get('country_code')[0] : '';
        $errors->has('first_name') ? $msg['first_name']     = $errors->get('first_name')[0] : '';
        $errors->has('last_name') ? $msg['last_name']       = $errors->get('last_name')[0] : '';
        $errors->has('email') ? $msg['email']               = $errors->get('email')[0] : '';
       

        return $msg;
    }


    public function validateUpdateData($data, &$errors) 
    {

        $validator = Validator::make($data, $this->updateRules);

        if($validator->fails()) {
            $errors = $this->formatUpdateErrors($validator->errors());
            return false;
        }

        return true;
    } 




    public function updateUser($user, $data, &$resData)
    {
        $this->updateUserRules($user->id);
        if(!$this->validateUpdateData($data, $errors)) {
            $resData = $errors;
            return false;
        }


        if(isset($data['mobile_no']) && isset($data['country_code'])) {

            $dupMob = $this->user->where('country_code', $data['country_code'])
                        ->where('mobile_no', $data['mobile_no'])
                        ->whereNotIn('id', [$user->id])
                        ->first();

            if($dupMob) {
                $resData = ['mobile_no' => 'Moblie number is already in use'];
                return false;
            } 

            $user->country_code = $data['country_code'];
            $user->mobile_no = $data['mobile_no'];
            $user->is_mobile_verified = 0;

        }


        if(isset($data['password'])) {
            $user->password = Hash::make($data['password']);
        }


        if(isset($data['first_name'])) {
            $user->first_name = $data['first_name'];
        }

        if(isset($data['last_name'])) {
            $user->last_name = $data['last_name'];
        }


        if(isset($data['email'])) {
            $user->email = $data['email'];
        }

        $user->save();

        $resData = ['user' => $user];
        return true;

    }


}