<?php

namespace App\Repositories;

use App\Models\Admin as AdminModel;



class Admin
{

	public function __construct(AdminModel $admin)
	{
		$this->admin = $admin;
	}



	public function allAdmins()
	{
		return $this->admin->all();
	}



	/**
	*	Used to delete by admin id
	*/
	public function deleteAdminByID($authAdmin, $adminID)
	{
		/**
		*	checking if the same admin trying to delete himself or not
		*/
		if($authAdmin->id == $adminID) {
			return false;
		}

		$admin = $this->admin->find($adminID);

		if(!$admin)	{
			return false;
		}


		$admin->delete();
		return true;

	}




	/**
	*	Used to update admin by id and passing new data
	*/
	public function updateAdmin($adminID, $data)
	{
		$admin = $this->admin->find($adminID);

		if(!$admin) {
			return false;
		} 


		foreach($data as $key => $value) {
			$admin->$key = $value;
		}

		try {
			$admin->save();
			$admin->touch();
		} catch(\Exception $e){
			return false;
		}
		
		return true;

	}



}