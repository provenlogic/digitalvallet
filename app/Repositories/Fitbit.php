<?php

namespace App\Repositories;

use App\Models\FitbitUser;
use App\Models\FitbitUserStepGoal;
use App\Models\Order;
use App\Models\User;
use DB;


class Fitbit
{

	public function __construct(
		User $user, 
		FitbitUser $fitbitUser, 
		FitbitUserStepGoal $fitbitUserStepGoal,
		Order $order
	) 
	{
		$this->fitbitUser = $fitbitUser;
		$this->fitbitUserStepGoal = $fitbitUserStepGoal;
		$this->user = $user;
		$this->order = $order;
	}



	/**
	*	map fitbit userid with database user id
	*/
	public function mapFitbitWithUser($userId, $fitbitID, $refresh_token)
	{
		$fitbitUser = $this->fitbitUser->where('fitbit_user_id', $fitbitID)->first();

		if(!$fitbitUser) {
			
			$fitbitUser = new $this->fitbitUser;
		}

		$fitbitUser->user_id = $userId;
		$fitbitUser->fitbit_user_id = $fitbitID;
		$fitbitUser->fitbit_refresh_token = $refresh_token;
		$fitbitUser->save();

		return $fitbitUser;

	}


	public function getFitbitUserRow($fitbitID)
	{
		return $this->fitbitUser->where('fitbit_user_id', $fitbitID)->first();
	}





	public function getUserStepGoal($user_id, $date)
	{
		$stepGoal = $this->fitbitUserStepGoal->where('user_id', $user_id)->where('step_log_date', $date)->first();

		if(!$stepGoal) {

			$stepGoal = new $this->fitbitUserStepGoal;;
			$stepGoal->user_id = $user_id;
			$stepGoal->step_log_date = $date;
			$stepGoal->step_count = 0;
			$stepGoal->save();
		}

		return $stepGoal;

	}





	public function transferAmountToSavings($user, $amount, $extrainfo = '')
	{

		$amount = app('App\Repositories\Balance')->formatAmountDecimalTwo($amount);

		DB::beginTransaction();

		try {

			$order = new $this->order;
			$order->user_id = $user->id;
			$order->other_user_id = 0;
			$order->trans_table_id = 0;
			$order->amount = $amount;
			$order->order_type = 'SAVINGS_ADDED';
			$order->remarks = 'Fitbit step goal amount added to savings';
			$order->status = 'SUCCESS';
			$order->status_reason = 'ADD_AMOUNT_TO_SAVINGS_FOR_FITBIT_STEP_GOAL';
			$order->save();
			

			$user->balance -= $amount;
			$user->saving_amount += $amount;
			$user->save();

			DB::commit();

			$msg = $this->msgReplace(
				[
					'{{amount}}'      => $amount,
					'{{currency}}'    => $user->currency,
					'{{extrainfo}}'   => $extrainfo
				],
				config('otp_messages.setp_goal_trasfer_amount_to_saving')
			);
			app('App\Repositories\Otp')->sendMessage(
				$user->fullMobileno(), $msg
			);

			return true;

		} catch(\Exception $e){
			DB::rollback();
			return false;
		}


	}



	protected function msgReplace($data = [], $msg)
	{
		foreach($data as $key => $value) {
			$msg = str_replace($key, $value, $msg);	
		}
   		return $msg;
	}



}
