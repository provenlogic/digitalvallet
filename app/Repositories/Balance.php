<?php

namespace App\Repositories;

/**
* Balance class
* 
* This class is part of DigitalWallet product. 
* Used for common balance and transaction related methods
*
* @package    Digital Wallet
* @author     provenlogic
*/


use DB;
use App\Repositories\Email;
use App\Repositories\GatewayAbstract;
use App\Models\Order;
use App\Repositories\Otp;
use App\Models\Transaction;
use App\Models\User;
use Validator;


class Balance
{

	/**
	* initialize the the dependencies
	*
	* @param object $user  User model
	* @param object $order  Order model
	* @param object $email  Email model
	* @param object $otp  Otp model
	* @param object $user  Transaction model
	*/
	public function __construct(
		Order $order, 
		User $user, 
		Email $email, 
		Otp $otp, 
		Transaction $transaction
	)
	{
		$this->user  = $user;
		$this->email = $email;
		$this->otp   = $otp;
		$this->order = $order;
		$this->transaction = $transaction;
	}



	/**
	* used to transafer amount to other user account
	*
	* @param object $user reference for user model object
	* @param string $rMobile  receiver usre mobile number
	* @param stirng $countryCode  receiver user country code
	* @param stirng|float $amount  transfer amount
	* @param stirng $remarks  transaction comment for sender
	* @param array $error  reference object from caller method adds errors this array
	*
	* @return boolean if success true or false
	*/
	public function transferAmount(&$user, $rMobile, $countryCode, $amount, $remarks, &$error) 
	{
		$toUser = $this->getUserByMobileAndCountryCode($rMobile, $countryCode);
		$amount = $this->formatAmountDecimalTwo($amount);


		if($user->id == $toUser->id) {
			$error['type'] = 'SAME_NUMBER';
			$error['message'] = 'You are not supposed to put own number.';
			return false;
		}



		$order = new $this->order;
		$order->user_id = $user->id;
		$order->other_user_id = $toUser->id;
		$order->amount = $amount;
		$order->order_type = Order::SEND_MONEY;
		$order->remarks = $remarks;
		

		if($user->balance < $amount) {
			$error['type'] = 'LOW_BALANCE';
			$error['message'] = 'You have low balance';

			$order->status = Order::FAILED;
			$order->status_reason = 'LOW_BALANCE';
			$order->save();

			return false;
		}


		DB::beginTransaction();

		try {

			$user->balance -= $amount;
			$user->save();


			$toUser->balance += $amount;
			$toUser->save();


			$rOrder = new $this->order;
			$rOrder->user_id = $toUser->id;
			$rOrder->other_user_id = $user->id;
			$rOrder->amount = $amount;
			$rOrder->order_type = Order::ADD_MONEY;
			$rOrder->status = Order::SUCCESS;
			$rOrder->status_reason = 'ADD_BALANCE_SUCCESS_FROM_OTHER';
			$rOrder->save();


			$order->status = Order::SUCCESS;
			$order->status_reason = 'TRANSFERRED_SUCCESS';
			$order->save();

			DB::commit();

			$user->order_id = $order->id;

			$msg = $this->msgReplace(
				[
					'{{amount}}'   => $amount,
					'{{currency}}' => $user->currency,
					'{{mobile}}'   => $toUser->fullMobileno()
				],
				config('otp_messages.balance_transfer_sender')
			);
			$this->otp->sendMessage(
				$user->fullMobileno(), $msg
			);


			$msg = $this->msgReplace(
				[
					'{{amount}}'      => $amount,
					'{{currency}}'    => $toUser->currency,
					'{{mobile_to}}'   => $toUser->fullMobileno(),
					'{{mobile_from}}' => $user->country_code.$user->mobile_no,
					'{{name}}'        => $user->first_name
				],
				config('otp_messages.balance_transfer_receiver')
			);
			$this->otp->sendMessage(
				$toUser->fullMobileno(), $msg
			);


			return true;

		} catch(\Exception $e){
			DB::rollback();
			$error['type'] = 'SERVER_ERROR';
			$error['message'] = 'Server error, try after some time';
			return false;
		}
		
	}



	/**
	* takes string and associative array replace the matched key with value
	*
	* @param array $data associative array with key and value
	* @param string $msg message stirng to be replaced with 
	*
	* @return string formated and replaced value string
	*/
	protected function msgReplace($data = [], $msg)
	{
		foreach($data as $key => $value) {
			$msg = str_replace($key, $value, $msg);	
		}
   		return $msg;
	}



	/**
	* used to get user by mobile and country code from user table
	*
	* @param string $mobile mobile number stored in users table
	* @param string $countryCode country code stored in users table
	*
	* @return user model object
	*/
	protected function getUserByMobileAndCountryCode($mobile, $countryCode)
	{
		return $this->user->where('mobile_no', $mobile)
                    ->where('country_code', $countryCode)
                    ->first();
	}



	/**
	* used to format amount to decimal two place precision
	*
	* @param numeric string|number $amount amount to be formated(3 -> 3.00)
	*
	* @return number formated as two decimal place(3.00)
	*/
	public function formatAmountDecimalTwo($amount = "")
	{
		return number_format($amount, 2, '.', '');
	}




	/**
	* used to validate user input data for balance transfer (see transferAmount() method)
	*
	* @param array user input data as associative array
	* @param reference adds the error messages with field value(associative array)
	*
	* @return boolean
	*/
	public function validateBalanceTransferData($data, &$errMsgs = []) 
	{

		$validator = Validator::make($data, [
			'amount' => 'required|regex:/^[0-9]*[.]?[0-9]*$/',
			'country_code' => 'required|regex:/^[0-9]+$/',
			'receiver_mobile' => 'required|exists:users,mobile_no|mobile_rule'
		]);


		if($validator->fails()) {

			$e = $validator->errors();
			($e->has('amount')) ? $errMsgs['amount'] = $e->get('amount')[0] : '';
			($e->has('country_code')) ? $errMsgs['country_code'] = $e->get('country_code')[0] : '';
			($e->has('receiver_mobile')) ? $errMsgs['receiver_mobile'] = $e->get('receiver_mobile')[0] : '';

			return false;
		}

		return true;

	}




	/**
	* Used to create laravel custom validation rule
	*
	* This method is called from AppServiceProvider to register custom mobile rule
	* when user inputs country code and mobile number as seperate field then takes both 
	* country code and mobile number and search for a spicific user
	* because country code and mobile number are saved as seperate column in users table.
	* Mobile no can be duplicate across the world for uniqness of a particular user search
	*
	*/
	public static function registerCustomMobileCountryCodeRule()
	{
		Validator::extend('mobile_rule', function($attribute, $value, $parameters) {
            
            return User::where('mobile_no', $value)
                    ->where('country_code', request()->country_code)
                    ->exists();
        });
	}






	public function addbalance($data, &$responseData)
	{

		$gateway = GatewayAbstract::instance($data['gateway']);

		if(isset($data['amount'])) {
			$data['amount'] = $this->formatAmountDecimalTwo($data['amount']);
		}
	
		$processedData = $gateway->charge($data);

		$processedData['amount'] = $this->formatAmountDecimalTwo($processedData['amount']);

		$t = new $this->transaction;
		$t->trans_id = $processedData['transaction_id'];
		$t->amount = $processedData['amount'];
		$t->gateway = $data['gateway'];
		$t->extra_info = json_encode($processedData['extra']);
		$t->status = $processedData['status'];



		if($processedData['success']){

			$already = $this->transaction
				->where('trans_id', $processedData['transaction_id'])
				->exists();

			if($already) {
				$responseData = ['Dot try to be smart'];
				return false;
			}

			

			DB::beginTransaction();

			$t->save();

			$order = new $this->order;
			$order->user_id = $data['auth_user']->id;
			$order->trans_table_id = $t->id;
			$order->other_user_id = 0;
			$order->amount = $processedData['amount'];
			$order->order_type = Order::ADD_MONEY;
			$order->remarks = isset($data['remarks']) ? $data['remarks'] : '';
			$order->status = Order::SUCCESS;
			$order->status_reason = 'ADD_BALANCE_SUCCESS_WITH_STRIPE';
			$order->save();

			DB::commit();

			$user = $data['auth_user'];
			$user->balance += $processedData['amount'];
			$user->save();


			$msg = $this->msgReplace(
				[
					'{{amount}}'   => $processedData['amount'],
					'{{currency}}' => $user->currency
				],
				config('otp_messages.balance_add')
			);
			$this->otp->sendMessage(
				$user->fullMobileno(), $msg
			);

			$responseData = ['balance' => $user->balance, 'order' => '#'.$order->id];
			return true;

		} else {

			DB::beginTransaction();

			$t->save();

			$order = new $this->order;
			$order->user_id = $data['auth_user']->id;
			$order->trans_table_id = $t->id;
			$order->other_user_id = 0;
			$order->amount = $processedData['amount'];
			$order->order_type = Order::ADD_MONEY;
			$order->remarks = isset($data['remarks']) ? $data['remarks'] : '';
			$order->status = Order::FAILED;
			$order->status_reason = 'ADD_BALANCE_FAILED_WITH_STRIPE';
			$order->save();

			DB::commit();

	
			$user = $data['auth_user'];
			$this->otp->sendMessage(
				$user->fullMobileno(), config('otp_messages.balance_add_failed')
			);

			$responseData = ['balance' => $user->balance, 'order' => '#'.$order->id];
			return false;

		}


	}



	protected function orderType(&$order)
	{
		switch ($order->order_type) {

			case 'ADD_MONEY':
				return $order->trans_table_id == 0 ? 'RECEIVED' : 'ADDED';
				break;

			case 'SEND_MONEY':
				return 'PAID';
				break;

			case 'SAVINGS_ADDED':
				return 'SAVINGS_ADDED';
				break;

			case 'BANK_TRANSFER':
				# code...
				break;

		}
	}


	public function getTransactionDetails($user)
	{

		$orders = $this->order
					->with('receiver')
					->with('transaction')
					->where('user_id', $user->id)
					->orderBy('created_at', 'DESC')
					->paginate(100);


		$details = [
			'transactions' => [],
			'paging' => $this->createPagination('api/user/transactions', $orders)
		];
		

		foreach($orders as $order) {

			$detail['order_id'] = $order->id;
			$detail['status'] = $order->status;
			$detail['amount'] = $order->amount;
			$detail['currency'] = $order->currency;
			$detail['remarks'] = $order->remarks;
			$detail['timestamp'] = $order->created_at->toDateTimeString();
			$detail['timestamp_format'] = 'UTC';
			$detail['t_type'] = $this->orderType($order);
			$detail['gateway'] = $order->transactionGateway();

			if($order->receiver) {
				$detail['other_user_name'] = $order->receiver->first_name.' '.$order->receiver->last_name;
				$detail['other_mobile_no'] = $order->receiver->fullMobileno();
			}

				

			$details['transactions'][] = $detail;
 
		}



		return $details;

	}





	protected function createPagination($route, $lengthAwarepaginator)
	{
		$paging = [
            "total" => $lengthAwarepaginator->total(),
            "more_pages" => $lengthAwarepaginator->hasMorePages(),
            "prevous_page_url" => "",
            "next_page_url"    => ""
        ];

        $cur_page = $lengthAwarepaginator->currentPage();

        $last_page = $lengthAwarepaginator->lastPage();

        $next_page_url = ($last_page > $cur_page) ? url($route.'?page='.($cur_page+1)) : "";
        $paging['next_page_url'] = $next_page_url;

        if ($cur_page > 1) {
            $paging['prevous_page_url'] = url($route.'?page='.($cur_page-1));
        } else {
            $paging['prevous_page_url'] = "";
        }

        return $paging;
	}




	/**
	* used to get all orders details
	*
	* @param integer $take how many records each page
	*
	* @return laravel pagination collection of order objects
	*/
	public function getAllOrders($take)
	{
		$orders = $this->order
						->with('user')
						->with('transaction')
						->with('receiver')
						->orderBy('orders.created_at', 'desc')
						->paginate($take);


		foreach ($orders as $key => $order) {
		      if (is_null($order->user)) {
		          $orders->pull($key);
		      }
		}

	
		$orders->getCollection()->transform(function ($order) {
		
			return $this->formatOrderData($order);
		
		});




		return $orders;
	}





	/**
	*	used to get order details by order id
	*
	* @param integer $id orders table if for order model
	*
	* @return return the Order model object
	*/
	public function getOrderById($id)
	{
		return $this->order->with('user')->with('transaction')->with('receiver')->find($id);
	}





	/**
	*	used to add new data or format data like amount for view purpose
	*
	* @param Order object $order Order model object
	*
	* @return return the modified formated order object
	*/
	public function formatOrderData($order)
	{

		$order->user_name = $order->user->fullName();
		$order->user_mobile = $order->user->fullMobileno();


		$tr_id = $order->transactionID();
		$order->transaction_id = ($tr_id == '' || $tr_id == 'X-X-X-X-X') ? '----' : $tr_id;

		$order->gateway = $order->transactionGateway() ? $order->transactionGateway() : '----';

		$order->amount_string = '';
		$order->amount_deducted = false;
		switch ($order->order_type) {
			case 'ADD_MONEY':
				$order->amount_string = '+'. $order->amount . ' $';
				break;

			case 'SEND_MONEY':
				$order->amount_string = '-'. $order->amount . ' $';
				$order->amount_deducted = true;
				break;
			case 'SAVINGS_ADDED':
				$order->amount_string = '-'.$order->amount.' $';
				$order->amount_deducted = true;
				break;
		}

		$order->type = $this->orderType($order);
		$order->transaction_date = $order->transactionDate();

		$order->other_user = false;

		if($order->receiver){
			$order->other_user = true;
			$order->other_user_name = $order->receiver->fullName();
			$order->other_user_mobile = $order->receiver->fullMobileno();	
		}

		return $order;

	}



}