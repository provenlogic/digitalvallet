<?php

use App\Repositories\Setting;

class Senang extends GatewayAbstract
{

	public function __construct(Setting $setting)
	{
		$this->setting = $setting;
	}



	public function gatewayName()
	{
		return 'SENANGPAY';
	}


	public function charge($data = [])
	{
		
		try {

          	$senang = Senangpay::create([
            	"amount" => $data['amount']*100,
            	"currency" => $data['auth_user']->currency,
            	"source" => $charge_token,
            	"description" => $data['description']
            ]);


          	return [
				'success'        => $senang->paid,
				'transaction_id' => $senang->balance_transaction,
				'status'         => $senang->status,
				'amount'         => $senang->amount/100,
				'extra'          => $senang->__toArray(true)
          	];



        } catch(\Exception $e) {

        	return [
				'success'        => false,
				'transaction_id' => 'X-X-X-X-X',
				'status'         => 'failed',
				'amount'         => $data['amount'],
				'extra'          => [
					'try_catch_error_message' => $e->getMessage()
				]
          	];
        }

 
	}


}


?>