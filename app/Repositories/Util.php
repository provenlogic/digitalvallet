<?php

namespace App\Repositories;

class Util
{

	public function timeMinuteAgo($timeStamp, $minuteAgo) 
	{
   		$to_time = gmdate("Y-m-d H:i:s", time());
        $minute = $this->timeDiffMinutes($timeStamp, $to_time);
        return ($minute <= $minuteAgo) ? true : false;
   	}


   	public function timeDiffMinutes($startTimeStamp, $endTimestamp)
   	{
   		$from_time = strtotime($startTimeStamp);
        $to_time = strtotime($endTimestamp);
       	return round(($to_time - $from_time) / 60);
   	}

   	public function generateFourDRandomNum()
   	{
   		return rand(1000, 9999);
   	}


    public function addMinutesToTimestamp($timestamp, $min)
    {
        return date('Y-m-d H:i:s', strtotime('+ '.$min.' minutes', strtotime($timestamp) ));
    }

    public function subMinutesToTimestamp($timestamp, $min)
    {
        return date('Y-m-d H:i:s', strtotime('- '.$min.' minutes', strtotime($timestamp) ));
    }

    public function msgReplace($data = [], $msg)
    {
        foreach($data as $key => $value) {
            $msg = str_replace($key, $value, $msg); 
        }
        return $msg;
    }

}