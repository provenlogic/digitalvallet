<?php

namespace App\Repositories;


/**
* Api class
* 
* This class is part of DigitalWallet product. 
* Used for token based authentication methods and tokens api response base structure
*
* @package Digital Wallet
* @author  provenlogic
*/


use App\Models\AccessToken;
use App\Models\User;


class Api
{

	/**
	* initialize the the dependencies
	*
	* @param object $user  User model
	* @param object $accessToken  AccessToken model
	*/
	public function __construct(User $user, AccessToken $accessToken)
	{
		$this->user = $user;
		$this->accessToken = $accessToken;

		$this->userTableName = $this->user->getTableName();
		$this->accessTokenTableName = $this->accessToken->getTableName();
	}



	/**
	* parse and extract access token from Authorization header value
	*
	* @param stirng $authorizationHeader  authorization token header value
	* @return string authentication token
	*/
	public function getAccessToken($authorizationHeader)
	{
		return str_replace('Bearer ', "", $authorizationHeader);
	}


	/**
	* creates and saves the auth token for a specific user id
	*
	* @param stirng|integer $userID id of the user or admin id
	* @param stirng $eType  token entity type. default is USER
	* @return object AccessToken access token object
	*/
	public function createAndSaveAccessToken($userID, $eType = 'USER') 
	{
		$accessToken = new $this->accessToken;
		$accessToken->entity_id = $userID;
		$accessToken->token = $userID.'.'.$this->createAccessToken();
		$accessToken->entity_type = $eType;
		$accessToken->save();
		return $accessToken;
	}


	/**
	* deletes all access token for a specified user
	*
	* @param stirng|integer $userID id of the user or admin id
	* @param stirng $eType  token entity type. default is USER
	* @return boolean throw exception if error happened
	*/
	public function removingAllAccesstokens($userID, $eType = 'USER')
	{
		$this->accessToken
				->where('entity_id', $userID)
				->where('entity_type', $eType)
				->delete();
		return true;
	}




	/**
	* creates random string (used for access token)
	*
	* @return string random string
	*/
	protected function createAccessToken()
	{
		return md5(uniqid(mt_rand(), true)).md5(uniqid(mt_rand(), true));
	}


	/**
	* used to check user authentication accesstoken and retrive the user
	*
	* @param stirng $access_token access token
	* @return object $user User model object
	*/
	public function shouldPassThrough($access_token) 
	{

		if(is_null($access_token) 
            || empty($access_token)
        ) {
            return false;
        }

    
		$user = $this->user->join(
			$this->accessTokenTableName, $this->userTableName.'.id', 
			'=', 
			$this->accessTokenTableName.'.entity_id'
		)
		->where($this->accessTokenTableName.'.token', $access_token)
		->where($this->accessTokenTableName.'.entity_type', 'USER')
		->where($this->accessTokenTableName.'.status', 'VALID')
		->select(['users.*'])
		->first();

		return $user;
	}



	/**
	* return json response for unknown error
	*
	* @return return json response object string with prefilled for unknown error response
	*/
	public function unknownErrResponse()
	{
		return response()->json(
			$this->createResponse(false, 'UNKNOWN_ERROR', 'Unknown error')
		);
	}



	/**
	* used to create response array structure for all api responses
	*
	* @param boolean $success (if api call success then true or false)
	* @param stirng $type type of the api response
	* @param stirng $text text of the api response
	* @param mixed $data default empty array
	* @return array
	*/
	public function createResponse($success = true, $type = "", $text = "", $data = [])
	{
		return [
			'success' => $success,
			'type' => $type,
			'text' => $text,
			'data' => $data
		];
	}


}