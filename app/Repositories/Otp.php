<?php

namespace App\Repositories;

use App\Models\OtpToken;
use App\Repositories\Setting;
use Aloha\Twilio\Twilio;
use App\Repositories\Util;


class Otp 
{

	public function __construct(OtpToken $otpToken, Util $util, Setting $setting)
	{
		$this->otpToken = $otpToken;
		$this->util = $util;
		$this->setting = $setting;
	}


	public function twilioSid()
	{
		return $this->setting->get('twilio_sid');
		//return config('twilio.twilio.connections.twilio.sid');
	}

	public function twilioToken()
	{
		return $this->setting->get('twilio_token');
		//return config('twilio.twilio.connections.twilio.token');
	}

	public function twilioFrom()
	{
		return $this->setting->get('twilio_from');
		//return config('twilio.twilio.connections.twilio.from');
	}


	public function sendMessage($mobileNo, $message, &$error = null)
	{
		try{

			$twilio = new Twilio($this->twilioSid(), $this->twilioToken(), $this->twilioFrom());
			$twilio->message($mobileNo, $message);

      	} catch(\Exception $e){
             $error = $e->getMessage();
             return false;
        }

        return true;
	}



	public function sendOTP($type, $mobileNo, $message, &$error = null, $user)
	{
        $otp = new $this->otpToken;
    
        $otp->mobile_no = $mobileNo;
        $otp->token_type = $type;
        $otp->token = $this->generateOTPCode();
        $otp->expired_at = $this->util->addMinutesToTimestamp(date('Y-m-d H:i:s'), 10);
        $otp->save();

        app('App\Repositories\Email')->sendOtpEmail($user, $otp->token);

        $message = str_replace("{{otp_code}}", $otp->token, $message);

		try{

			$twilio = new Twilio($this->twilioSid(), $this->twilioToken(), $this->twilioFrom());
			$twilio->message($mobileNo, $message);

      	} catch(\Exception $e){
             $error = $e->getMessage();
             //return false;
             return true;
        }

        return $otp->token;
	}

	protected function generateOTPCode()
	{
		return $this->util->generateFourDRandomNum();
	}


	protected function otpExpired($startTimeString, $maxMin)
	{
		return !$this->util->timeMinuteAgo($startTimeString, $maxMin);
	}


	public function verifyOTP($type, $mobileNo, $otpCode)
	{
		$otp = $this->otpToken->where('mobile_no', $mobileNo)
					->where('token_type', $type)
                    ->where('token', $otpCode)
                    ->select(['id', 'expired_at'])
                    ->first();

        if(!$otp) { 
        	return false; 
       	}

       	$success = !$this->otpExpired($otp->expired_at, 10);

       	if($success) {
       		$otp->expired_at = $this->util->subMinutesToTimestamp(date('Y-m-d H:i:s'), 60);
       		$otp->save();
       	}

        return $success;
	}

}