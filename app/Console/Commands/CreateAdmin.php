<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Hash;


class CreateAdmin extends Command
{

    protected $signature = 'create-admin';
    protected $description = 'used to create admin';

    public function handle()
    { 
        $this->askAdminDetails();
        $this->createAdmin();
    }


    protected function createAdmin()
    {
        try {

            DB::table('admins')->insert([

                'name'         => $this->name,
                'email'        => $this->email,
                'password'     => $this->password,
                'role'         => $this->role,
                'purpose'      => $this->purpose,
                'country_code' => $this->countryCode,
                'mobile_no'    => $this->mobileno,
                'last_ip'      => $this->lastIP,
                'last_login'   => $this->lastLogin,
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s')
            ]);

            $this->info('Admin created successfully');

        } catch(\Exception $e) {
            $this->error('Admin create failed: '.$e->getMessage());
        }

    }


    protected function askAdminDetails()
    {
        $this->name = $this->ask('Enter admin name');
        $this->email = $this->ask('Enter admin email');
        $this->password = Hash::make($this->ask('Enter admin password'));
        $this->role = $this->ask('Enter admin role (root or guest)');
        $this->purpose = $this->ask('Enter admin purpose');
        $this->countryCode = '+'.$this->ask('Enter admin phone country code (without +)');
        $this->mobileno = $this->ask('Enter admin phone moblie no (without country code');
        $this->lastIP = request()->ip();
        $this->lastLogin = date('Y-m-d H:i:s');
  
    }

}
