<?php

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;



Route::get('offers', function(){

	return view('offers.index');

});
 
Route::get('user/senangpay/response', 'SenangController@getSenangpayResponse');

/* api routes */
Route::group(['prefix' => 'api'], function(){

	Route::post('user/register', 'RegisterController@doRegister');
	Route::post('user/login', 'RegisterController@doLogin');

	Route::post('user/register/google', 'GoogleController@doRegister');
	Route::post('user/register/facebook', 'FacebookController@doRegister');



	Route::post('user/password/otp/send', 'OtpController@sendPasswordChangeOtp');
	Route::post('user/password/change', 'OtpController@verifyOtpAndChangePassword');


	Route::group(['middleware' => 'api'], function(){

		Route::post('user/balance/transfer', 'BalanceController@transferBalance');
		Route::post('user/stripe/balance/add', 'BalanceController@addBalanceWithStripe');
		Route::post('user/paypal/balance/add', 'BalanceController@addBalanceWithPaypal');
		//Route::post('user/senangpay/balance/add', 'BalanceController@addBalanceWithSenangpay');
		Route::get('user/senangpay/details', 'SenangController@getDetails');
		Route::post('user/senangpay/init', 'SenangController@getSenangInit');
		// Route::get('user/senangpay/response', 'SenangController@getSenangpayResponse');
		



		Route::post('user/register/otp/send', 'OtpController@sendRegisterOtp');
		Route::post('user/register/otp/verify', 'OtpController@verifyRegisterOtp');


		Route::get('user/transactions', 'BalanceController@getTransactionDetails');


		Route::post('user/update', 'RegisterController@updateUser');
		Route::get('user/balance', 'BalanceController@getBalance');



		Route::get('user/savings', 'SavingController@getSavingDetails');



	});


});





/*fitbit related routes */
Route::get('fitbit/authorize/{user_id}', 'FitbitController@authoriseFitbitUser');
Route::get('fitbit/callback', 'FitbitController@authoriseFitbitCallback');
Route::get('fitbit/webhook/activities', 'FitbitController@verifyFitbitWebhook');
Route::post('fitbit/webhook/activities', 'FitbitController@fitbitActivitiesWebhook');
/*end of fitbit related routes*/








Route::group(['prefix' => 'admin'], function(){



	Route::get('login', 'Admin\LoginController@showLogin')->middleware('adminGuest');
	Route::post('login', 'Admin\LoginController@doLogin')->middleware('adminGuest');


	Route::group(['middleware' => 'admin'], function(){


		Route::get('logout', 'Admin\LoginController@logout');
		Route::get('dashboard', 'Admin\DashboardController@showDashboard');
		Route::get('users', 'Admin\UserManageController@showUsers');
		Route::post('users/status/change', 'Admin\UserManageController@changeUserStatus');
		Route::get('users/transactions', 'Admin\UserManageController@showUserTransactions');
		Route::get('user/order/{id}', 'Admin\UserManageController@getOrderDetails');


		Route::get('admin-management', 'Admin\AdminManageController@showAdmins');
		Route::post('admin-management/admin/update', 'Admin\AdminManageController@updateAdmin');
		Route::post('admin-management/admin/delete', 'Admin\AdminManageController@deleteAdmin');
		/*Route::get('settings/admin/create', 'Admin\AdminManageController@createAdmin');*/



		Route::get('settings/twilio', 'OtpController@showTwilioSettings');
		Route::post('settings/twilio/save', 'OtpController@saveTwilioSettings');
		Route::post('settings/twilio/test', 'OtpController@testTwilioSms');


		Route::get('settings/facebook', 'FacebookController@showFacebookSettings');
		Route::post('settings/facebook/save', 'FacebookController@saveFacebookSettings');
		Route::post('settings/facebook/test', 'FacebookController@testFacebook');


		Route::get('settings/stripe', 'Admin\StripeSettingsController@showStripeSettings');
		Route::post('settings/stripe/save', 'Admin\StripeSettingsController@saveStripeSettings');
		Route::post('settings/stripe/test', 'Admin\StripeSettingsController@testStripe');

		Route::get('settings/paypal', 'Admin\PaypalSettingsController@showPaypalSettings');
		Route::post('settings/paypal/save', 'Admin\PaypalSettingsController@savePaypalSettings');

		Route::get('settings/senangpay', 'Admin\SenangpaySettingsController@showSenangpaySettings');
		Route::post('settings/senangpay/save', 'Admin\SenangpaySettingsController@saveSenangpaySettings');



		Route::get('settings/email', 'Admin\MailSettingsController@showMailSettings');
		Route::post('settings/email/save', 'Admin\MailSettingsController@saveMailServerDetails');
		Route::post('settings/email/test', 'Admin\MailSettingsController@testMail');
		
		Route::get('settings/fitbit', 'FitbitController@showSettings');
		Route::post('settings/fitbit/save', 'FitbitController@saveFitbitSettings');


		Route::get('settings/ifttt', 'IFTTTController@showSettings');
		Route::post('settings/ifttt/fitbit/activity-recipe/save', 'IFTTTController@saveFITBITActivityRecipeSave');
		Route::post('settings/ifttt/email-recipe/save', 'IFTTTController@saveEmailRecipeSetting');


	});


});





/*
* * make angular routes working
*/
Route::get('/', function(){
	$allSettings = app('App\Repositories\Setting')->all();
	return view('web_user.index', compact('allSettings'));
});

Route::get('{web}/{segments?}', function($web, $segments = ''){
	$allSettings = app('App\Repositories\Setting')->all();
	return view('web_user.index',compact('allSettings'));
})->where('web', '^(?!api|admin).*$')->where('segments', '.*');

//*******  REMEMBER : IN USER TABLE REMOVE THE EMAIL UNIQUE SIGN *********
//*******  CHANGES  : EMAIL VALIDATION ADDED TO "RegisterRepository.php" ********







/* testing fitbit apis */


/*Route::post('ifttt/webhooktest', function(\Illuminate\Http\Request $request){

	\Log::info('ifttt webhooktest');
	\Log::info($request->all());

	$setting = app('App\Repositories\Setting');
	
     
	$user = app('App\Models\User')->where('mobile_no', $setting->get('ifttt_fitbit_mobile_no'))->where('country_code', $setting->get('ifttt_fitbit_country_code'))->first();

	app('App\Repositories\Fitbit')->transferAmountToSavings($user, $setting->get('ifttt_fitbit_amount'), '(ifttt)');

	app('\App\Repositories\Otp')->sendMessage('+919093036897', json_encode($request->all()));
	

	return response()->json([], 204);

});*/

/*Route::get('ifttt/webhooktest/email', function(\Illuminate\Http\Request $request){

	\Log::info('ifttt webhooktest email');
	\Log::info($request->all());

	$setting = app('App\Repositories\Setting');

	if($request->subject == $setting->get('ifttt_email_subject')) {

		$user = app('App\Models\User')->where('mobile_no',$setting->get('ifttt_email_mobile_no'))->where('country_code', $setting->get('ifttt_email_mobile_country_code'))->first();
	
		app('App\Repositories\Otp')->sendMessage(
			$user->fullMobileno(), $setting->get('ifttt_email_sms_body')
		);


	}


	return response()->json([], 204);

});*/





/*Route::get('fitbit/get-data', function(\Illuminate\Http\Request $request){

	$provider = new \djchen\OAuth2\Client\Provider\Fitbit([
	    'clientId'          => '228JYV',
	    'clientSecret'      => '947fc27a2eefffa60d441b769d8ca29e',
	    'redirectUri'       => 'http://localhost/digitalvallet/public/fitbit/callback'
	]);


    try {

     

         $accessToken = $provider->getAccessToken('refresh_token', [
	        'refresh_token' => $request->refresh_token
	    ]);
dd($provider);

        $request = $provider->getAuthenticatedRequest(

            \djchen\OAuth2\Client\Provider\Fitbit::METHOD_POST,
            \djchen\OAuth2\Client\Provider\Fitbit::BASE_FITBIT_API_URL . '/1/user/-/activities/date/'.date('Y-m-d').'.json',
            $accessToken,
            [
            	'headers' => [\djchen\OAuth2\Client\Provider\Fitbit::HEADER_ACCEPT_LANG => 'en_US'], 
            				[\djchen\OAuth2\Client\Provider\Fitbit::HEADER_ACCEPT_LOCALE => 'en_US']
            ]
            // Fitbit uses the Accept-Language for setting the unit system used
            // and setting Accept-Locale will return a translated response if available.
            // https://dev.fitbit.com/docs/basics/#localization
        );


    
        $response = $provider->getParsedResponse($request);

		dd($response);

	 } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

        // Failed to get the access token or user details.
        dd('Error: ' . $e->getMessage());

    }


});*/






