<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Setting;

class IFTTTController extends Controller
{


    public function __construct(Api $api, Setting $setting)
    {
        $this->api = $api;
        $this->setting = $setting;

    }


    /**
    *   shows admin panel settings page
    */
    public function showSettings()
    {
        return view('admin.ifttt_setting', [
            'ifttt_fitbit_mobile_no'          => $this->setting->get('ifttt_fitbit_mobile_no'),
            'ifttt_fitbit_country_code'       => $this->setting->get('ifttt_fitbit_country_code'),
            'ifttt_fitbit_amount'             => $this->setting->get('ifttt_fitbit_amount'),
            'ifttt_email_mobile_no'           => $this->setting->get('ifttt_email_mobile_no'),
            'ifttt_email_mobile_country_code' => $this->setting->get('ifttt_email_mobile_country_code'),
            'ifttt_email_subject'             => $this->setting->get('ifttt_email_subject'),
            'ifttt_email_sms_body'            => $this->setting->get('ifttt_email_sms_body')

        ]);
    }




    /**
    *   saves ifttt fitbit activity recipe settings
    */
    public function saveFITBITActivityRecipeSave(Request $request)
    {
        $this->setting->set('ifttt_fitbit_mobile_no', $request->ifttt_fitbit_mobile_no);
        $this->setting->set('ifttt_fitbit_country_code', $request->ifttt_fitbit_country_code);
        $this->setting->set('ifttt_fitbit_amount', $request->ifttt_fitbit_amount);
        return response()->json(
            $this->api->createResponse(true, 'IFTTT_FITBIT_ACTIVITY_SETTINGS_SAVED', 'IFTTT Fitbit activity settings saved')
        );
    }





    /**
    *   saves ifttt email recipe settings
    */
    public function saveEmailRecipeSetting(Request $request)
    {
        $this->setting->set('ifttt_email_mobile_no', $request->ifttt_email_mobile_no);
        $this->setting->set('ifttt_email_mobile_country_code', $request->ifttt_email_mobile_country_code);
        $this->setting->set('ifttt_email_subject', $request->ifttt_email_subject);
        $this->setting->set('ifttt_email_sms_body', $request->ifttt_email_sms_body);
        return response()->json(
            $this->api->createResponse(true, 'IFTTT_EMAIL_RECIPE_SETTINGS_SAVED', 'IFTTT email recipe settings saved')
        );
    }





    /**
    *   saves fitbit admin panel setttings
    */
    public function saveFitbitSettings(Request $request)
    {
        $this->setting->set('fitbit_client_id', $request->fitbit_client_id);
        $this->setting->set('fitbit_client_secret', $request->fitbit_client_secret);
        $this->setting->set('fitbit_subscription_webhook_verify_code', $request->fitbit_subscription_webhook_verify_code);
        $this->setting->set('fitibt_steps_count', $request->fitibt_steps_count);
        $this->setting->set('fitibt_steps_goal_amount', $request->fitibt_steps_goal_amount);

        return response()->json(
        $this->api->createResponse(true, 'FITBIT_SETTINGS_SAVED', 'Fitbit settings saved')
        );
    }




}
