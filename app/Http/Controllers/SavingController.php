<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Repositories\Balance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SavingController extends Controller
{


    public function __construct(Balance $balance, Api $api)
    {
        $this->balance = $balance;
        $this->api = $api;
    }




    public function getSavingDetails(Request $request)
    {
        return response()->json(
            $this->api->createResponse(true, 'SAVINGS_DETAILS_RETRIVED', 'Savings details retrived successfully', [
                'saving_amount' => (float)$request->auth_user->saving_amount
            ])
        );
    }



}
