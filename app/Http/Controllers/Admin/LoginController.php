<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class LoginController extends Controller
{

    public function showLogin()
    {
        return view('admin.login');
    }




    public function doLogin(Request $request)
    {

    	$success = Auth::admin()->attempt([
    		'email' => $request->email, 'password' => $request->password
    	], false);

    	if($success) {
    		return redirect()->intended('admin/dashboard');
    	} else {
    		return redirect('admin/login')->with('l_status', 'Login failed');
    	}

    }




    public function logout()
    {
        Auth::admin()->logout();
		return redirect('admin/login');
    }



}
