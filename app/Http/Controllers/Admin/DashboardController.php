<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\User;


class DashboardController extends Controller
{

	public function __construct(User $user)
	{
		$this->authAdmin = Auth::admin()->get();
		$this->user = $user;
	}

    public function showDashboard()
    {
        
        return view('admin.dashboard', [
        	'total_users_count' => $this->user->getTotalUsersCount(),
        	'total_this_month_users_count' => $this->user->getUsersCountByCreatedDate(date('Y-m')),
        	'total_today_users_count' => $this->user->getUsersCountByCreatedDate(date('Y-m-d')),
        	'country_signups' => $this->user->getCountrySignUps()
        ]);

    }


}
