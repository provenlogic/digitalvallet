<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Mail;
use Illuminate\Http\Request;
use App\Repositories\Setting;


class MailSettingsController extends Controller
{

    public function __construct(Setting $setting, Api $api)
    {
        $this->setting = $setting;
        $this->api = $api;
    }

  

    public function showMailSettings()
    {

        return view('admin.email_settings', [
            'mail_driver'       => $this->setting->get('mail_driver'),
            'smtp_host'         => $this->setting->get('smtp_host'),
            'smtp_port'         => $this->setting->get('smtp_port'),
            'smtp_username'     => $this->setting->get('smtp_username'),
            'smtp_password'     => $this->setting->get('smtp_password'),
            'smtp_from_address' => $this->setting->get('smtp_from_address'),
            'smtp_from_name'    => $this->setting->get('smtp_from_name'),
            'smtp_encryption'   => $this->setting->get('smtp_encryption'),
            'mandrill_host'      => $this->setting->get('mandrill_host'),
            'mandrill_port'      => $this->setting->get('mandrill_port'),
            'mandrill_username'  => $this->setting->get('mandrill_username'),
            'mandrill_secret'    => $this->setting->get('mandrill_secret'),
        ]);

    }




    public function saveMailServerDetails(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);

        foreach($data as $key => $value) {
            $this->setting->set($key, $value);
        }
        

        return response()->json(
            $this->api->createResponse(true, 'MAIL_SETTINGS_SAVED', 'Mail Settings saved')
        );

    }



    /**
    *   used to test email after setting email config in admin panel
    */
    public function testMail (Request $request) {

        $email_id = $request->email_id;
        $body = $request->body;
        
        try {

            Mail::raw($body, function ($message) use($email_id) {
                $message->to($email_id, 'Test Mail User')->subject("Test Mail");
            });

            return response()->json(
                $this->api->createResponse(true, 'MAIL_SENT', 'Mail sent successfully')
            );


        } catch (\Exception $e) {

            return response()->json(
                $this->api->createResponse(false, 'MAIL_SENT_FAILED', 'Mail send failed', ["message" => $e->getMessage()])
            );
        }
            

    }



}
