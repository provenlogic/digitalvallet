<?php 

namespace App\Http\Controllers\Admin;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Setting;

/**
* 
*/
class SenangpaySettingsController extends Controller
{
	
	public function __construct(Setting $setting, Api $api)
    {
        $this->setting = $setting;
        $this->api = $api;
    }

  

    public function showSenangpaySettings()
    {
        return view('admin.senangpay_settings', [
            'senangpay_merchant_id'   => $this->setting->get('senangpay_merchant_id'),
            'senangpay_secret_key' => $this->setting->get('senangpay_secret_key')
        ]);
    }



    public function saveSenangpaySettings(Request $request)
    {
        $this->setting->set('senangpay_merchant_id', $request->senangpay_merchant_id);
        $this->setting->set('senangpay_secret_key', $request->senangpay_secret_key);
        return response()->json(
            $this->api->createResponse(true, 'SENANGPAY_SETTINGS_SAVED', 'Senangpay settings saved')
        );
    }
}


?>