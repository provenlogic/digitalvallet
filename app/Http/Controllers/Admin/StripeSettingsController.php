<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Setting;
use App\Repositories\Stripe;


class StripeSettingsController extends Controller
{

    public function __construct(Setting $setting, Api $api, Stripe $stripe)
    {
        $this->setting = $setting;
        $this->api = $api;
        $this->stripe = $stripe;
    }

  

    public function showStripeSettings()
    {
        return view('admin.stripe_settings', [
            'stripe_api_server_key'   => $this->setting->get('stripe_api_server_key'),
            'stripe_api_public_key' => $this->setting->get('stripe_api_public_key')
        ]);
    }



    public function saveStripeSettings(Request $request)
    {
        $this->setting->set('stripe_api_server_key', $request->stripe_api_server_key);
        $this->setting->set('stripe_api_public_key', $request->stripe_api_public_key);
        return response()->json(
            $this->api->createResponse(true, 'STRIPE_SETTINGS_SAVED', 'Stripe settings saved')
        );
    }



    public function testStripe(Request $request)
    {
        $data = $this->stripe->testCharge($request->all());
        return response()->json(
            $this->api->createResponse(true, 'TESTED', 'Stripe settings tested', $data)
        );
    }



}
