<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Repositories\Api;
use App\Repositories\Balance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\User;


class UserManageController extends Controller
{

	public function __construct(User $user, Balance $balance, Api $api)
	{
		$this->authAdmin = Auth::admin()->get();
		$this->user = $user;
        $this->balance = $balance;
        $this->api = $api;
	}

    public function showUsers(Request $request)
    {
        $take = $request->has('take') ? $request->take : 100;
        $users = $this->user->getUsers($take);
        return view('admin.users', ['users' => $users]);

    }




    public function changeUserStatus(Request $request)
    {

        $userIds = explode(',', $request->user_ids);
        $userIds = $userIds == "" ? [] : $userIds;

        if($request->action_type == 'ACTIVATE_USER'){

            $this->user->activateUsers($userIds);
            return response()->json([
                'success' => true, 
                'text' => 'Users activated successfully'
            ]);

        } else if($request->action_type == 'DEACTIVATE_USER') {

            $this->user->deActivateUsers($userIds);
            return response()->json([
                'success' => true, 
                'text' => 'Users deactivated successfully'
            ]);
        
        } else if($request->action_type == 'DELETE_USERS') {
            $this->user->deleteUsers($userIds);
            return response()->json([
                'success' => true, 
                'text' => 'Users deleted successfully'
            ]);
        }


    }







    public function showUserTransactions(Request $request)
    {
        $take = $request->has('take') ? $request->take : 100;
        $orders = $this->balance->getAllOrders($take);
        return view('admin.user_transactions', [
            'orders' => $orders
        ]);
    }




    public function getOrderDetails(Request $request)
    {
        $order = $this->balance->getOrderById($request->id);
        $order = $this->balance->formatOrderData($order);
        return response()->json(
            $this->api->createResponse(true, 'ORDER', 'Order fetched', ['order' => $order])
        );
    }


}

