<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use App\Repositories\Email;
use Illuminate\Http\Request;
use App\Repositories\SocialLogin;
use App\Repositories\Setting;
use Socialite;

class FacebookController extends Controller
{


    public function __construct(SocialLogin $socialLogin, Api $api, Email $email, Setting $setting)
    {
        $this->socialLogin = $socialLogin;
        $this->api = $api;
        $this->email = $email;
        $this->setting = $setting;

        $this->socialLogin->setProvider(SocialLogin::FACEBOOK);
        $this->setFacebookConfig();
    }


     /**
    *   This is to init faceboo api config dynamically
    */
    protected function setFacebookConfig()
    {
        config([
            'services.facebook.client_id' => $this->setting->get('facebook_app_id'),
            'services.facebook.client_secret' => $this->setting->get('facebook_app_secret'),
        ]);
    }



    /**
    *   This is to show admin panel facbeook api key settings
    */
    public function showFacebookSettings(Request $request)
    {
        return view('admin.facebook_settings', [
            'facebook_app_id'   => $this->setting->get('facebook_app_id'),
            'facebook_app_secret' => $this->setting->get('facebook_app_secret'),
        ]);
    }



    public function saveFacebookSettings(Request $request)
    {

        $this->setting->set('facebook_app_id', $request->facebook_app_id);
        $this->setting->set('facebook_app_secret', $request->facebook_app_secret);
        return response()->json(
            $this->api->createResponse(true, 'FACEBOOK_SETTINGS_SAVED', 'Facebook settings saved')
        );

    }


    /**
    *   This is to test facbeook user data with access token
    */
    public function testFacebook(Request $request)
    {
        try {

            $user = Socialite::driver('facebook')->userFromToken($request->auth_token);

        } catch(\Exception $e) {
            $user = null;
        }

        return response()->json(
            $this->api->createResponse(true, 'TESTED', 'Tested', ['user' => $user])
        );

    }





    
    public function doRegister(Request $request)
    {

        if(is_null($request->auth_token) || empty($request->auth_token)) {
            return response()->json(
                $this->api->createResponse(false, 'AUTH_TOKEN_REQUIRED', 'Auth token required')
            );
        }


        try {
            $user = Socialite::driver('facebook')->userFromToken($request->auth_token);
        } catch(\Exception $e) {
            return $this->api->unknownErrResponse();
        }
        

        $email = isset($user->email) ? $user->email : "";
        $storedUser = $this->socialLogin->getStoredUser($user->id, $email);

        if($storedUser) {
           
            $accessToken = $this->api->createAndSaveAccessToken($storedUser->id);

            return response()->json(
                $this->api->createResponse(
                    true, 'LOGGED_IN', 'User logged in successfully', [
                    'access_token' => $accessToken->token,
                    'is_data_incomplete' => $this->socialLogin->isDataIncomplete(),
                    'user' => $storedUser
                ])
            );
        }


        $fname = "";
        $lname = "";
        if(isset($user->name)) {
            $nameArr = explode(" ", $user->name);
            $fname = isset($nameArr[0]) ? $nameArr[0] : "";
            $lname = isset($nameArr[1]) ? $nameArr[1] : "";
        }
            

        $newUser = $this->socialLogin
            ->setSocialID($user->id)
            ->setUserEmail($user->email)
            ->setFirstname($fname)
            ->setLastname($lname)
            ->registerUser();


        if(!$newUser) {
            return $this->api->unknownErrResponse();
        }

        $this->email->sendRegistrationEmail($newUser);
        $accessToken = $this->api->createAndSaveAccessToken($newUser->id);

        return response()->json(
            $this->api->createResponse(
                true, 'REGISTERED', 'User registered successfully', [
                'access_token' => $accessToken->token,
                'is_data_incomplete' => $this->socialLogin->isDataIncomplete(),
                'user' => $newUser
            ])
        );


    }

}
