<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use App\Repositories\Email;
use Illuminate\Http\Request;
use App\Repositories\SocialLogin;
use Socialite;

class GoogleController extends Controller
{


    public function __construct(SocialLogin $socialLogin, Api $api, Email $email)
    {
        $this->socialLogin = $socialLogin;
        $this->api = $api;
        $this->email = $email;

        $this->socialLogin->setProvider(SocialLogin::GOOGLE);
    }

    
    public function doRegister(Request $request)
    {

        if(is_null($request->auth_token) || empty($request->auth_token)) {
            return response()->json(
                $this->api->createResponse(false, 'AUTH_TOKEN_REQUIRED', 'Auth token required')
            );
        }


        try {
            $user = Socialite::driver('google')->userFromToken($request->auth_token);
        } catch(\Exception $e) {
            return $this->api->unknownErrResponse();
        }
        

        $storedUser = $this->socialLogin->getStoredUser($user->id, $user->email);

        if($storedUser) {
           
            $accessToken = $this->api->createAndSaveAccessToken($storedUser->id);

            return response()->json(
                $this->api->createResponse(
                    true, 'LOGGED_IN', 'User logged in successfully', [
                    'access_token' => $accessToken->token,
                    'is_data_incomplete' => $this->socialLogin->isDataIncomplete(),
                    'user' => $storedUser
                ])
            );
        }


        $nameArr = explode(" ", $user->name);
        $fname = isset($nameArr[0]) ? $nameArr[0] : "";
        $lname = isset($nameArr[1]) ? $nameArr[1] : "";

        $newUser = $this->socialLogin
            ->setSocialID($user->id)
            ->setUserEmail($user->email)
            ->setFirstname($fname)
            ->setLastname($lname)
            ->registerUser();


        if(!$newUser) {
            return $this->api->unknownErrResponse();
        }

        $this->email->sendRegistrationEmail($newUser);
        $accessToken = $this->api->createAndSaveAccessToken($newUser->id);

        return response()->json(
            $this->api->createResponse(
                true, 'REGISTERED', 'User registered successfully', [
                'access_token' => $accessToken->token,
                'is_data_incomplete' => $this->socialLogin->isDataIncomplete(),
                'user' => $newUser
            ])
        );


    }

}
