<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Http\Controllers\Controller;
use App\Repositories\Otp;
use Illuminate\Http\Request;
use App\Repositories\Setting;
use App\Repositories\User;

class OtpController extends Controller
{


    public function __construct(Otp $otp, Api $api, User $user, Setting $setting)
    {
        $this->otp = $otp;
        $this->api = $api;
        $this->user = $user;
        $this->setting = $setting;
    }



    public function testTwilioSms(Request $request)
    {
        $success = $this->otp->sendMessage($request->mobile_no, $request->message, $error);
        if($success) {
            return response()->json(
                $this->api->createResponse(true, 'SMS_SENT', 'SMS sent successfully')
            );
        } else {
            return response()->json(
                $this->api->createResponse(false, 'SMS_SENT_FAILED', $error)
            );
        }
    }



    public function showTwilioSettings(Request $request)
    {
        return view('admin.twilio_setting', [
            'twilio_sid'   => $this->setting->get('twilio_sid'),
            'twilio_token' => $this->setting->get('twilio_token'),
            'twilio_from'  => $this->setting->get('twilio_from')
        ]);
    }




    public function saveTwilioSettings(Request $request)
    {
        $this->setting->set('twilio_sid', $request->twilio_sid);
        $this->setting->set('twilio_token', $request->twilio_token);
        $this->setting->set('twilio_from', $request->twilio_from);
        return response()->json(
            $this->api->createResponse(true, 'TWILIO_SETTINGS_SAVED', 'Twilio settings saved')
        );
    }



 
   
    public function sendRegisterOtp(Request $request)
    {

        $auth_user = $request->auth_user;

       	if($otpToken = $this->otp->sendOTP(
       		'REGISTRATION', 
       		$auth_user->fullMobileno(),
       		config('otp_messages.registration'),
       		$error,
            $auth_user
       	)) {


            /*app('App\Repositories\Email')->sendOtpEmail($auth_user, $otpToken);*/



       		return response()->json(
       			$this->api->createResponse(true, "OTP_SENT", "OTP sent successfully")
       		);
        } else {

       		return response()->json(
       			$this->api->createResponse(false, "OTP_SEND_FAILED", "OTP send failed")
       		);

       }


    }



    public function verifyRegisterOtp(Request $request)
    {

        $auth_user = $request->auth_user;


        $success = $this->otp->verifyOTP(
            'REGISTRATION', 
            $auth_user->fullMobileno(), 
            $request->otp_code
        );


        if($success) {
            $auth_user->is_mobile_verified = 1;
            $auth_user->save();

            return response()->json(
                $this->api->createResponse(true, "OTP_VERIFIED", "OTP verified successfully",[
                    'user' => $auth_user
                ])
            );
        } else {

            return response()->json(
                $this->api->createResponse(false, "OTP_VERIFY_FAILED", "OTP verification failed")
            );

        }



    }







    public function sendPasswordChangeOtp(request $request)
    {

        $mobile_no = $request->mobile_no;
        $country_code = '+'.str_replace('+', '', $request->country_code);
        $full_mobile = $country_code.$mobile_no;


        $user = $this->user->findUserByMobile($country_code, $mobile_no);

     

        if(!$user) {
            return response()->json(
                $this->api->createResponse(false, "MOBILE_NOT_REGISTERED", "Mobile number not registered with us")
            );
        }

       

        if($otpToken = $this->otp->sendOTP(
            'REGISTRATION', 
            $full_mobile,
            config('otp_messages.password_recovery'),
            $error,
            $user
        )) {


            /*app('App\Repositories\Email')->sendOtpEmail($user, $otpToken);*/



            return response()->json(
                $this->api->createResponse(true, "OTP_SENT", "OTP sent successfully")
            );
        } else {

            return response()->json(
                $this->api->createResponse(false, "OTP_SEND_FAILED", "OTP send failed")
            );

       }

    }




    public function verifyOtpAndChangePassword(Request $request)
    {

        if($request->has('country_code')) {
            $request->request->add(['country_code' => '+'.$request->country_code]);
        }

        $success = $this->user->verifyOtpAndChangePassword(
            $request->new_password, 
            $request->otp_code,
            $request->mobile_no,
            $request->country_code,
            $resData
        );


        if($success) {

            return response()->json(
                $this->api->createResponse(true, "PASSWORD_CHANGED", 'Password changed successfully', $resData)
            );

        } else {

            return response()->json(
                $this->api->createResponse(false, "PASSWORD_CHANGE_FAILED", 'Password changed failed', $resData)
            );
        }



    }




}
