<?php

namespace App\Http\Controllers;

use App\Repositories\Api;
use App\Repositories\Fitbit as FitbitRepo;
use djchen\OAuth2\Client\Provider\Fitbit;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\Setting;

class FitbitController extends Controller
{


    public function __construct(FitbitRepo $fitbit, Api $api, Setting $setting)
    {
        $this->api = $api;
        $this->fitbit = $fitbit;
        $this->setting = $setting;

    }


    /**
    *   shows admin panel settings page
    */
    public function showSettings()
    {
        return view('admin.fitbit_setting', [
            'fitbit_client_id'                        => $this->setting->get('fitbit_client_id'),
            'fitbit_client_secret'                    => $this->setting->get('fitbit_client_secret'),
            'fitbit_subscription_webhook_verify_code' => $this->setting->get('fitbit_subscription_webhook_verify_code'),
            'fitibt_steps_count'                      => $this->setting->get('fitibt_steps_count'),
            'fitibt_steps_goal_amount'                => $this->setting->get('fitibt_steps_goal_amount')

        ]);
    }




    /**
    *   saves fitbit admin panel setttings
    */
    public function saveFitbitSettings(Request $request)
    {
        $this->setting->set('fitbit_client_id', $request->fitbit_client_id);
        $this->setting->set('fitbit_client_secret', $request->fitbit_client_secret);
        $this->setting->set('fitbit_subscription_webhook_verify_code', $request->fitbit_subscription_webhook_verify_code);
        $this->setting->set('fitibt_steps_count', $request->fitibt_steps_count);
        $this->setting->set('fitibt_steps_goal_amount', $request->fitibt_steps_goal_amount);

        return response()->json(
        $this->api->createResponse(true, 'FITBIT_SETTINGS_SAVED', 'Fitbit settings saved')
        );
    }






    /**
    *   redirects user to fitbit autorization page 
    */
    public function authoriseFitbitUser(Request $request)
    {

        if($request->user_id === '') {
            return response()->json('User id missing');
        }


        session(['user_id' => $request->user_id]);

        $provider = new Fitbit([
            'clientId'          => $this->setting->get('fitbit_client_id'),
            'clientSecret'      => $this->setting->get('fitbit_client_secret'),
            'redirectUri'       =>  url('fitbit/callback')
        ]);

        $authorizationUrl = $provider->getAuthorizationUrl();
        return redirect($authorizationUrl);

    }




    /**
    *   verify fitbit subscription webhook
    */
    public function verifyFitbitWebhook(Request $request)
    {
        
        if($request->has('verify') 
            && $request->verify === $this->setting->get('fitbit_subscription_webhook_verify_code')
        ) {

            return response()->json([], 204);
                
        }


        return response()->json([], 404);
    }









    /**
    *   fibit callback after user authrize fitbit app
    */
    public function authoriseFitbitCallback(Request $request)
    {


        $provider = new Fitbit([
            'clientId'          => $this->setting->get('fitbit_client_id'),
            'clientSecret'      => $this->setting->get('fitbit_client_secret'),
            'redirectUri'       =>  url('fitbit/callback')
        ]);

        $userID = session('user_id');


        try {

    
            $accessToken = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);

            $resourceOwner = $provider->getResourceOwner($accessToken);

            $this->fitbit->mapFitbitWithUser(
                $userID, $resourceOwner->toArray()['encodedId'], $accessToken->getRefreshToken()
            );


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://api.fitbit.com/1/user/".$resourceOwner->toArray()['encodedId']."/activities/apiSubscriptions/StepCount.json");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_POSTFIELDS,[]);  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = [
                'Authorization: Bearer '.$accessToken->getToken(),
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $server_output = curl_exec ($ch);

            curl_close ($ch);



            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://api.fitbit.com/1/user/".$resourceOwner->toArray()['encodedId']."/activities/apiSubscriptions/StepCount.json");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,[]);  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $headers = [
                'Authorization: Bearer '.$accessToken->getToken(),
            ];

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $server_output = curl_exec ($ch);

            curl_close ($ch);

            return response()->json(['success' => true]);


        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

            return response()->json(['error' => $e->getMessage(). ' or already subscribed or unkonwn error happened']);
        }




    }






    public function fitbitActivitiesWebhook(Request $request)
    {
        

        $user = null;
        $fitbitUser = null;
        $accessToken = null;
        $totalAmount = 0;

        foreach($request->all() as $data) {


            try {



           
                if(!$fitbitUser) {
                    $fitbitUser = $this->fitbit->getFitbitUserRow($data['ownerId']);
                }

                if(!$user) {
                    $user = app('App\Models\User')->find($fitbitUser->user_id);
                }


        
                


                if(!$accessToken) {

                    $provider = new Fitbit([
                        'clientId'          => $this->setting->get('fitbit_client_id'),
                        'clientSecret'      => $this->setting->get('fitbit_client_secret'),
                        'redirectUri'       =>  url('fitbit/callback')
                    ]);

                    $accessToken = $provider->getAccessToken('refresh_token', [
                        'refresh_token' => $fitbitUser->fitbit_refresh_token
                    ]);

                    $fitbitUser->fitbit_refresh_token = $accessToken->getRefreshToken();
                    $fitbitUser->save();

                    $accessToken = $accessToken->getToken();

                }



                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://api.fitbit.com/1/user/".$data['ownerId']."/activities/date/".date('Y-m-d').".json");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $headers = [
                    'Authorization: Bearer '.$accessToken,
                ];

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $server_output = curl_exec ($ch);

                curl_close ($ch);

                


                $resData = json_decode($server_output, true);

                if(!isset($resData['summary']['steps'])) {
                    continue;
                }


                $stepGoal = $this->fitbit->getUserStepGoal($user->id, $data['date']);


                $newStepsCount = $resData['summary']['steps'] - $stepGoal->step_count;



                $multiplier = $this->setting->get('fitibt_steps_count');
                $amountToBeAdded = $this->setting->get('fitibt_steps_goal_amount');

                $amountToBeAdded = $amountToBeAdded * (  (int)($newStepsCount / $multiplier)  );

                $remainder = (int)($newStepsCount % $multiplier);

                $stepGoal->step_count = $resData['summary']['steps'] - $remainder;
                $stepGoal->save();

                $totalAmount += $amountToBeAdded;

                \Log::info('steps: '. $resData['summary']['steps']);

            }catch(\Exception $e) {
                \Log::info($e->getMessage(). ' Line : ' . $e->getLine().' file : '.$e->getFile());
            }


        }



        \Log::info('total amount : '. $totalAmount);
        
        if($totalAmount != 0) {

            $this->fitbit->transferAmountToSavings($user, $totalAmount);

        }


        return response()->json([], 204);

    }









}
